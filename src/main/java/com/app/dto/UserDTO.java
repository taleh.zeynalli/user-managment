package com.app.dto;

import lombok.*;


import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable {

    private static final long serialVersionUID = 6835192601898364280L;
    private Long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String encryptedPassword;
    private String emailVerificationToken;
    private Boolean emailVerificationStatus = false;
    private List<AddressDTO> addresses;

//    @Override
//    public String toString() {
//        return "UserDTO{" +
//                "id=" + id +
//                ", userId='" + userId + '\'' +
//                ", firstName='" + firstName + '\'' +
//                ", lastName='" + lastName + '\'' +
//                ", email='" + email + '\'' +
//                ", password='" + password + '\'' +
//                ", encryptedPassword='" + encryptedPassword + '\'' +
//                ", emailVerificationToken='" + emailVerificationToken + '\'' +
//                ", emailVerificationStatus=" + emailVerificationStatus +
//                '}';
//    }
}
