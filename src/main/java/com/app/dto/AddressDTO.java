package com.app.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {
    private Long id;
    private String addressId;
    private String city;
    private String country;
    private String streetName;
    private String postalCode;
    private String type;
    private UserDTO users;

//    @Override
//    public String toString() {
//        return "AddressDTO{" +
//                "id=" + id +
//                ", addressId='" + addressId + '\'' +
//                ", city='" + city + '\'' +
//                ", country='" + country + '\'' +
//                ", streetName='" + streetName + '\'' +
//                ", postalCode='" + postalCode + '\'' +
//                ", type='" + type + '\'' +
//                '}';
//    }
}
