package com.app.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = 1875781911555655080L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userId;
    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String email;
    private String password;
    private String encryptedPassword;
    private String emailVerificationToken;
    @Column(columnDefinition = "boolean default false")
    private Boolean emailVerificationStatus = false;
    @OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
    private List<Address> addresses;

//    @Override
//    public String toString() {
//        return "User{" +
//                "id=" + id +
//                ", userId='" + userId + '\'' +
//                ", firstName='" + firstName + '\'' +
//                ", lastName='" + lastName + '\'' +
//                ", email='" + email + '\'' +
//                ", password='" + password + '\'' +
//                ", encryptedPassword='" + encryptedPassword + '\'' +
//                ", emailVerificationToken='" + emailVerificationToken + '\'' +
//                ", emailVerificationStatus=" + emailVerificationStatus +
//                '}';
//    }
}
