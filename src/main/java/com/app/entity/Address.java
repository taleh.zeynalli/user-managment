package com.app.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "adress")
public class Address implements Serializable {

    private static final long serialVersionUID = 7143268212746431532L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String addressId;
    private String city;
    private String country;
    private String streetName;
    private String postalCode;
    private String type;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_id", referencedColumnName = "id")
    private User users;
//
//    @Override
//    public String toString() {
//        return "Address{" +
//                "id=" + id +
//                ", addressId='" + addressId + '\'' +
//                ", city='" + city + '\'' +
//                ", country='" + country + '\'' +
//                ", streetName='" + streetName + '\'' +
//                ", postalCode='" + postalCode + '\'' +
//                ", type='" + type + '\'' +
//                '}';
//    }
}
