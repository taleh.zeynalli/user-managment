package com.app.controller;

import com.app.dto.AddressDTO;
import com.app.dto.UserDTO;
import com.app.mapper.UserMapper;
import com.app.model.RequestOperationName;
import com.app.model.RequestOperationStatus;
import com.app.model.request.UserDetailsRequestModel;
import com.app.model.response.AddressResponseModel;
import com.app.model.response.OperationStatusModel;
import com.app.model.response.UserResponse;
import com.app.service.AddressService;
import com.app.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.app.mapper.AddressMapper;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AddressMapper addressMapper;
    private final AddressService addressService;

    @GetMapping(value = "/{userId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public UserResponse getUser(@PathVariable("userId") String userId) {

        UserDTO userDTO = userService.getUserByUserId(userId);
        UserResponse response = userMapper.inheritUserResponse(userDTO);
        return response;

    }

    @GetMapping
    public List<UserResponse> getUser(@RequestParam("page") int page, @RequestParam("size") int size) {

        List<UserResponse> userResponseList = new ArrayList<>();
//        List<UserDTO> userList = userService.getAllUsers();

        userResponseList = userService.getAllUsers(page, size)
                .stream()
                .map(userMapper::inheritUserResponse)
                .collect(Collectors.toList());

        return userResponseList;

    }

    @PostMapping
    public UserResponse signUpUser(@RequestBody UserDetailsRequestModel user) {

        UserDTO userDTO = userMapper.inheritUserDTO(user);
        UserDTO createdUser = userService.save(userDTO);
        UserResponse userResponse = userMapper.inheritUserResponse(createdUser);

        return userResponse;
    }

    @PutMapping(value = "/{userId}")
    public UserResponse updateUser(@PathVariable("userId") String userId, @RequestBody UserDetailsRequestModel user) {

        UserDTO userDTO = userMapper.inheritUserDTO(user);
        UserDTO createdUser = userService.updateUser(userId, userDTO);
        UserResponse userResponse = userMapper.inheritUserResponse(createdUser);

        return userResponse;
    }

    @GetMapping("/{userId}/adresses")
    public List<AddressResponseModel> getAdresses(@PathVariable("userId") String userID) {

        List<AddressDTO> addressDTOList = addressService.getAddresses(userID);

        List<AddressResponseModel> list = addressDTOList
                .stream()
                .map(addressMapper::addresDTOToAddressResponse)
                .collect(Collectors.toList());

        return list;

    }

    @GetMapping("/{userId}/adresses/{addressId}")
    public AddressResponseModel getAdress(@PathVariable("userId") String userID,
                                          @PathVariable("addressId") String addressId) {

        AddressDTO addressDTO = addressService.getAddress(userID, addressId);
        AddressResponseModel address = addressMapper.addresDTOToAddressResponse(addressDTO);

        return address;

    }

    /*
     * http://localhost:8080/mobile-app-ws/users/email-verification?token=sdfsdf
     * */
    @GetMapping(path = "/email-verification", produces = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
    public OperationStatusModel verifyEmailToken(@RequestParam(value = "token") String token) {

        OperationStatusModel returnValue = new OperationStatusModel();
        returnValue.setOperationName(RequestOperationName.VERIFY_EMAIL.name());

        boolean isVerified = userService.verifyEmailToken(token);

        if(isVerified)
        {
            returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
        } else {
            returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
        }

        return returnValue;
    }

}
