package com.app.exception;

import com.app.model.response.ErrorMessages;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserServiceException extends RuntimeException {

    private String message;
    private String description;


    public UserServiceException(ErrorMessages errorMessages) {
        this.message = errorMessages.getErrorMessges();
        this.description = errorMessages.getDescription();
    }

    public UserServiceException(String message, String description) {
        this.message = message;
        this.description = description;

    }

}
