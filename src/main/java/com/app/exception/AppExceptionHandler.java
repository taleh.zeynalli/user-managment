//package com.app.exception;
//
//import com.app.model.response.ErrorResponse;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.context.request.WebRequest;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//
//@RestControllerAdvice
//public class AppExceptionHandler extends ResponseEntityExceptionHandler {
//
//    @ExceptionHandler(UserServiceException.class)
//    public ResponseEntity<Object> handleUserServiceException(UserServiceException ex, WebRequest webRequest){
//        ErrorResponse response = new ErrorResponse(ex.getMessage(),ex.getDescription());
//        return new ResponseEntity<>(response,new HttpHeaders(), HttpStatus.NOT_FOUND);
//    }
//
//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<Object> handleOtherException(Exception ex, WebRequest webRequest){
//        ErrorResponse response = new ErrorResponse(ex.getMessage());
//        return new ResponseEntity<>(response,new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
//    }
//
//}
