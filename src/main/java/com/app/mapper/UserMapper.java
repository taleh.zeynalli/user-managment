package com.app.mapper;

import com.app.dto.UserDTO;
import com.app.entity.User;
import com.app.model.request.UserDetailsRequestModel;
import com.app.model.response.UserResponse;
import org.mapstruct.*;


@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = AddressMapper.class)
public interface UserMapper {


    @Mapping(target = "addresses", source = "listAddreses")
    UserDTO inheritUserDTO(UserDetailsRequestModel userDetailsRequestModel);


    @Mapping(target = "addressResponseModelList", source = "addresses")
    UserResponse inheritUserResponse(UserDTO userDTO);


    User fromDTO2Entity(UserDTO userDTO, @Context CycleAvoidingMappingContext context);


    @Mapping(target = "addresses", source = "addresses")
    UserDTO fromEntity2DTO(User user);


}
