package com.app.mapper;

import com.app.dto.AddressDTO;
import com.app.dto.UserDTO;
import com.app.entity.Address;
import com.app.entity.User;
import com.app.model.response.AddressResponseModel;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AddressMapper {

//    @Mapping(target = "users",ignore = true)
//    Address DTO2Entiy(AddressDTO addressDTO);

//    @AfterMapping
//    default void ignore(Address address, @MappingTarget AddressDTO addressDTO) {
//        addressDTO.getUsers().setAddresses(null);
//    }

    @Mappings({
//            @Mapping(target = "users", ignore = true),
            @Mapping(target = "users", qualifiedByName = "userToUserDTO")
    })
    AddressDTO updateEntiy2DTO(Address address);

//    @Mappings({
////            @Mapping(target = "users", ignore = true),
//            @Mapping(target = "users" , qualifiedByName = "userDTOToUser")
//    })
//    Address updateDto2Entity( AddressDTO addressDTO);

    @Named("userToUserDTO")
    @Mappings({
            @Mapping(target = "addresses", expression = "java(null)")})
    UserDTO UserToUserDTO(User user);

    @Named("userDTOToUser")
    @Mappings({
            @Mapping(target = "addresses", expression = "java(null)")})
    User userDTOToUser(UserDTO userDTO);

    AddressResponseModel addresDTOToAddressResponse(AddressDTO address);
//    AddressDTO addresToAddressDTO(Address address);

}


