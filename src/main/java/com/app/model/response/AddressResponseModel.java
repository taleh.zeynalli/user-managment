package com.app.model.response;

import lombok.Data;

@Data
public class AddressResponseModel {

    private String addressId;
    private String city;
    private String country;
    private String streetName;
    private String postalCode;
    private String type;
}
