package com.app.model.response;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum ErrorMessages {

    MISSING_REQUIRED_FIELD("MISSING_REQUIRED_FIELD", "Missing required field. Please check documentation for required fields"),
    NO_RECORD_FOUND("NO_RECORD_FOUND", "No record found for provided id"),
    RECORD_ALREADY_EXISTS("RECORD_ALREADY_EXISTS", "Record already exists"),
    INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR", "Something went wrong. Please repeat this operation later.");

    private String errorMessges;
    private String description;

    ErrorMessages(String errorMessges, String description) {
        this.description = description;
        this.errorMessges = errorMessges;

    }


}
