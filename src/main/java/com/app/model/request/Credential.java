package com.app.model.request;

import lombok.Data;

@Data
public class Credential {

    private String email;
    private String password;
}
