package com.app.model.request;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class UserDetailsRequestModel {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private List<AddressRequestModel> listAddreses;


}
