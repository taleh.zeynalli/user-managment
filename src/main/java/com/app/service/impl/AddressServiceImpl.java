package com.app.service.impl;

import com.app.dto.AddressDTO;
import com.app.entity.Address;
import com.app.entity.User;
import com.app.repository.AddressRepository;
import com.app.repository.UserRepository;
import com.app.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.mapper.AddressMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final UserRepository userRepository;
    private final AddressRepository addressrepository;
    private final AddressMapper addressMapper;


    @Override
    public List<AddressDTO> getAddresses(String userId) {
        User user = userRepository.findByUserId(userId);
        List<AddressDTO> list = addressrepository.findAllByUsers(user).stream()
                .map(addressMapper::updateEntiy2DTO)
                .collect(Collectors.toList());

        return list;

    }

    @Override
    public AddressDTO getAddress(String userId, String addressId) {

        User user = userRepository.findByUserId(userId);

        AddressDTO addressDTO = user.getAddresses().stream()
                .filter(address -> address.getAddressId().equals(addressId))
                .findAny().map(addressMapper::updateEntiy2DTO).orElse(null);

        return addressDTO;
    }
}
