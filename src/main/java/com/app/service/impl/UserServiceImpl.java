package com.app.service.impl;

import com.app.dto.AddressDTO;
import com.app.dto.UserDTO;
import com.app.entity.Address;
import com.app.entity.User;
import com.app.exception.UserServiceException;
import com.app.mapper.AddressMapper;
import com.app.mapper.CycleAvoidingMappingContext;
import com.app.mapper.UserMapper;
import com.app.model.response.ErrorMessages;
import com.app.repository.UserRepository;
import com.app.service.UserService;
import com.app.util.Utils;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private Utils util;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDTO save(UserDTO userDTO) {

        User user = userRepository.findUserByEmail(userDTO.getEmail());
        if (Objects.nonNull(user)) throw new RuntimeException("User already exists");

        userDTO.getAddresses().forEach(addressDTO ->
        {
            addressDTO.setAddressId(util.generateAddressId(6));
            addressDTO.setUsers(userDTO);
        });


//        ModelMapper modelMapper = new ModelMapper();
//        user = modelMapper.map(userDTO, User.class);;

        user = userMapper.fromDTO2Entity(userDTO, new CycleAvoidingMappingContext());
        user.setUserId(util.generateUserId(6));
        user.setEncryptedPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setEmailVerificationToken(util.generateEmailVerificationToken(user.getUserId()));

        User savedUser = userRepository.save(user);
        UserDTO returnValue = userMapper.fromEntity2DTO(user);

        return returnValue;
    }

    @Override
    public UserDTO getUserByUserId(String userId) {

        UserDTO userDTO = new UserDTO();
        User user = userRepository.findByUserId(userId);
        if (Objects.isNull(user)) throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
        userDTO = userMapper.fromEntity2DTO(user);
        return userDTO;
    }

    @Override
    public UserDTO updateUser(String userId, UserDTO userDTO) {
        UserDTO userBean = new UserDTO();
        User user = userRepository.findByUserId(userId);
        if (Objects.isNull(user)) throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(user.getLastName());

        User savedUser = userRepository.save(user);
        UserDTO returnValue = userMapper.fromEntity2DTO(user);
        return returnValue;
    }

    @Override
    public List<UserDTO> getAllUsers(int page, int size) {

        List<UserDTO> userDTOList = new ArrayList<>();
        Pageable pageable = PageRequest.of(page, size);
        Page<User> usersPage = userRepository.findAll(pageable);

        userDTOList = usersPage.getContent().stream()
                .map(userMapper::fromEntity2DTO)
                .collect(Collectors.toList());

        return userDTOList;
    }

    @Override
    public boolean verifyEmailToken(String token) {

        boolean returnValue = false;

        // Find user by token
        User userEntity = userRepository.findUserByEmailVerificationToken(token);

        if (userEntity != null) {
            boolean hastokenExpired = Utils.hasTokenExpired(token);
            if (!hastokenExpired) {
                userEntity.setEmailVerificationToken(null);
                userEntity.setEmailVerificationStatus(Boolean.TRUE);
                userRepository.save(userEntity);
                returnValue = true;
            }
        }

        return returnValue;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findUserByEmail(email);
        if (Objects.isNull(user)) throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getEncryptedPassword(),
                user.getEmailVerificationStatus(),
                true, true,
                true, new ArrayList<>());

        //return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), new ArrayList<>());
//        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getEncryptedPassword(), new ArrayList<>());
    }
}
