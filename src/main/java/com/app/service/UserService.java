package com.app.service;

import com.app.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    UserDTO save(UserDTO userDTO);

    UserDTO getUserByUserId(String userID);

    UserDTO updateUser(String userId, UserDTO userDTO);

    List<UserDTO> getAllUsers(int page, int size);

    boolean verifyEmailToken(String token);
}
