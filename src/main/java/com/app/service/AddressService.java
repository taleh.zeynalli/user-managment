package com.app.service;

import com.app.dto.AddressDTO;

import java.util.List;

public interface AddressService {

    List<AddressDTO> getAddresses(String userID);

    AddressDTO getAddress(String userId, String addressId);
}
