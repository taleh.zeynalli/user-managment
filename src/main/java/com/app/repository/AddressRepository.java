package com.app.repository;

import com.app.entity.Address;
import com.app.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long> {

    List<Address> findAllByUsers(User user);

}
